package com.devcamp.restapicustomervisit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiCustomerVisitApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestApiCustomerVisitApplication.class, args);
	}

}
