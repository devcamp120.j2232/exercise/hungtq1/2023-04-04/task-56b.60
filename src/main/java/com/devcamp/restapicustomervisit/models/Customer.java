package com.devcamp.restapicustomervisit.models;

public class Customer {
  String name;
  Boolean member = false;
  String memberType;

  public Customer(String name, Boolean member, String memberType) {
    this.name = name;
    this.member = member;
    this.memberType = memberType;
  }

  public String getName() {
    return name;
  }

  public Boolean isMember() {
    return member;
  }

  public void setMember(Boolean member) {
    this.member = member;
  }

  public String getMemberType() {
    return memberType;
  }

  public void setMemberType(String memberType) {
    this.memberType = memberType;
  }

  @Override
  public String toString() {
    return "Customer [name=" + name + ", member=" + member + ", memberType=" + memberType + "]";
  }

}
