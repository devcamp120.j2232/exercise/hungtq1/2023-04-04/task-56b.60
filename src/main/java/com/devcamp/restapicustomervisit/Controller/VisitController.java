package com.devcamp.restapicustomervisit.Controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.restapicustomervisit.models.Customer;
import com.devcamp.restapicustomervisit.models.Visit;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class VisitController {
  @GetMapping("/visits")
  public ArrayList<Visit> getListVisits() {
    ArrayList<Visit> listVisits = new ArrayList<>();
    Customer customer1 = new Customer("Nguyen Van A", true, "Vip");
    Customer customer2 = new Customer("Le Thi B", true, "normal");
    Customer customer3 = new Customer("Pham Chi C", true, "normal");
    System.out.println(customer1.toString());
    System.out.println(customer2.toString());

    // SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    // date = dateFormat.parse("08-04-2023");
    Visit visit1 = new Visit(customer1, date, 350000.0, 100000.0);
    Visit visit2 = new Visit(customer2, date, 250000.0, 50000.0);
    Visit visit3 = new Visit(customer3, date, 200000.0, 50000.0);
    System.out.println(visit1.toString());
    System.out.println(visit2.toString());

    listVisits.add(visit1);
    listVisits.add(visit2);
    listVisits.add(visit3);

    return listVisits;
  }
}
